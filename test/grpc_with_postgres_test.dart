import 'dart:io';

import 'package:grpc_with_postgres/grpc_with_postgres.dart';
import 'package:test/test.dart';
import 'package:postgres/postgres.dart';

void main() {
  PostgreSQLConnection connection;
  EchoService echoService;

  setUpAll(() async {
    connection = PostgreSQLConnection(
      Platform.environment['POSTGRES_HOST'],
      int.tryParse(const String.fromEnvironment('port', defaultValue: '5432')),
      Platform.environment['POSTGRES_DB'],
      username: Platform.environment['POSTGRES_USER'],
      password: Platform.environment['POSTGRES_PASSWORD'],
    );
    await connection.open();

    await connection
        .query('CREATE TABLE echo(id serial PRIMARY KEY, message TEXT)');

    echoService = EchoService(connection);
  });

  tearDownAll(() async {
    await connection.close();
  });

  test('test insert', () async {
    await connection.query(
      'INSERT INTO echo (message) VALUES (@message)',
      substitutionValues: {
        'message': 'Hello world',
      },
    );

    final result = await connection.query('SELECT message FROM echo');
    expect(result.length == 1, isTrue);
    expect(result.first[0], 'Hello world');
  });

  test('test echo', () async {
    final testMessage = 'Hello world';
    await echoService.echo(null, Echo()..message = testMessage);

    final result = await connection.query('SELECT message FROM echo');
    expect(result.length == 2, isTrue);
    expect(result[1][0], testMessage);
  });
}
