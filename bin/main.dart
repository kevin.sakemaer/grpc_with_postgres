import 'dart:io';

import 'package:grpc/grpc.dart';
import 'package:grpc_with_postgres/grpc_with_postgres.dart';
import 'package:postgres/postgres.dart';

main(List<String> arguments) async {
  final connection = PostgreSQLConnection(
    Platform.environment['POSTGRES_HOST'],
    int.tryParse(Platform.environment['PORT'] ?? '5432'),
    Platform.environment['POSTGRES_DB'],
    username: Platform.environment['POSTGRES_USER'],
    password: Platform.environment['POSTGRES_PASSWORD'],
  );

  final echoService = EchoService(connection);
  final services = [
    echoService,
  ];
  final server = Server(services);

  await server.serve();
  print('Listening to ${server.port}');
}
