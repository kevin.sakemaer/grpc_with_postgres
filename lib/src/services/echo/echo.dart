import 'dart:async';

import 'package:grpc/grpc.dart';
import 'package:postgres/postgres.dart';
import 'package:grpc_with_postgres/src/generated/echo.pbgrpc.dart';

class EchoService extends EchoServiceBase {
  final PostgreSQLConnection connection;

  EchoService(this.connection);

  @override
  Future<Echo> echo(ServiceCall call, Echo request) async {
    await connection.query(
      'INSERT INTO echo (message) VALUES (@message)',
      substitutionValues: {
        'message': request.message,
      },
    );
    return Echo()..message = 'Echo: ${request.message}';
  }
}
