///
//  Generated code. Do not modify.
//  source: echo.proto
///
// ignore_for_file: non_constant_identifier_names,library_prefixes,unused_import

// ignore: UNUSED_SHOWN_NAME
import 'dart:core' show int, bool, double, String, List, override;

import 'package:protobuf/protobuf.dart' as $pb;

class Echo extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('Echo', package: const $pb.PackageName('grpc_with_postgres'))
    ..aOS(1, 'message')
    ..hasRequiredFields = false
  ;

  Echo() : super();
  Echo.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  Echo.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  Echo clone() => new Echo()..mergeFromMessage(this);
  Echo copyWith(void Function(Echo) updates) => super.copyWith((message) => updates(message as Echo));
  $pb.BuilderInfo get info_ => _i;
  static Echo create() => new Echo();
  static $pb.PbList<Echo> createRepeated() => new $pb.PbList<Echo>();
  static Echo getDefault() => _defaultInstance ??= create()..freeze();
  static Echo _defaultInstance;
  static void $checkItem(Echo v) {
    if (v is! Echo) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }

  String get message => $_getS(0, '');
  set message(String v) { $_setString(0, v); }
  bool hasMessage() => $_has(0);
  void clearMessage() => clearField(1);
}

