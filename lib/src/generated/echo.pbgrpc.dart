///
//  Generated code. Do not modify.
//  source: echo.proto
///
// ignore_for_file: non_constant_identifier_names,library_prefixes,unused_import

import 'dart:async' as $async;

import 'package:grpc/grpc.dart';

import 'echo.pb.dart';
export 'echo.pb.dart';

class EchoServiceClient extends Client {
  static final _$echo = new ClientMethod<Echo, Echo>(
      '/grpc_with_postgres.EchoService/echo',
      (Echo value) => value.writeToBuffer(),
      (List<int> value) => new Echo.fromBuffer(value));

  EchoServiceClient(ClientChannel channel, {CallOptions options})
      : super(channel, options: options);

  ResponseFuture<Echo> echo(Echo request, {CallOptions options}) {
    final call = $createCall(_$echo, new $async.Stream.fromIterable([request]),
        options: options);
    return new ResponseFuture(call);
  }
}

abstract class EchoServiceBase extends Service {
  String get $name => 'grpc_with_postgres.EchoService';

  EchoServiceBase() {
    $addMethod(new ServiceMethod<Echo, Echo>(
        'echo',
        echo_Pre,
        false,
        false,
        (List<int> value) => new Echo.fromBuffer(value),
        (Echo value) => value.writeToBuffer()));
  }

  $async.Future<Echo> echo_Pre(ServiceCall call, $async.Future request) async {
    return echo(call, await request);
  }

  $async.Future<Echo> echo(ServiceCall call, Echo request);
}
